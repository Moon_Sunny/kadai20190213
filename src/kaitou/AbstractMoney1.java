package kaitou;

import kadai.Money1;

public abstract class AbstractMoney1 implements Money1 {
    private double value;

    public AbstractMoney1(double value) {
        this.value = value;
    }

    public String toString() {
        return getPrefix() + this.value + getPostfix();
    }

    protected abstract String getPrefix();

    protected abstract String getPostfix();

    public double getValue() {
        return value;
    }
}