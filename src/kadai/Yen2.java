package kadai;

import kaitou.AbstractMoney2;

public class Yen2 extends AbstractMoney2 {
	public Yen2(double value) {
		super(value);
	}
	@Override
	protected String getPrefix() {
		return "";
	}
	@Override
	protected String getPostfix() {
		return "円";
	}
	@Override
	public double getRate() {
		return 1.0;
	}
	public static void setYenRate(double r) {
	}
}
