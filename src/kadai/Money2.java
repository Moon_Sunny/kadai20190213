package kadai;

public interface Money2 extends Money1 {
	double getRate();
	Money2 getYen();
}
