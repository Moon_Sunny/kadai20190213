package kadai;

import kaitou.AbstractMoney1;

public class Yen1 extends AbstractMoney1 {
	public Yen1(double value) {
		super(value);
	}
	@Override
	protected String getPrefix() {
		return "";
	}
	@Override
	protected String getPostfix() {
		return "円";
	}
}
