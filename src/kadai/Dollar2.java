package kadai;

import kaitou.AbstractMoney2;

public class Dollar2 extends AbstractMoney2 {
	private static double rate;
	public Dollar2(double value) {
		super(value);
	}
	@Override
	protected String getPrefix() {
		return "$";
	}
	@Override
	protected String getPostfix() {
		return "";
	}
	@Override
	public double getRate() {
		return rate;
	}
	public static void setYenRate(double r) {
		rate=r;
	}
}
