package kadai;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class Dollar1Test {
	@Test
	public void testToString() {
		Money1 m1 = new Dollar1(100);
		assertEquals("$100.0",m1.toString());
	}
}
