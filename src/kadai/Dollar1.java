package kadai;

import kaitou.AbstractMoney1;

public class Dollar1 extends AbstractMoney1 {

	public Dollar1(double value) {
		super(value);
	}
	@Override
	protected String getPrefix() {
		return "$";
	}
	@Override
	protected String getPostfix() {
		return "";
	}

}
