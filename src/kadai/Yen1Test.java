package kadai;

import static org.junit.Assert.*;

import org.junit.Test;

public class Yen1Test {

	@Test
	public void testToString() {
		Money1 m1 = new Yen1(100);
		assertEquals("100.0円",m1.toString());
	}
}
