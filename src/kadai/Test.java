package kadai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kadai.Dollar3;
import kadai.Euro;
import kadai.Money3;
import kadai.Yen3;

public class Test {
	public static void main(String[] args) {
		List<Money3> moneyList = new ArrayList<Money3>();
		moneyList.add(new Yen3(100));
		moneyList.add(new Yen3(150));
		moneyList.add(new Yen3(200));
		moneyList.add(new Dollar3(1));
		moneyList.add(new Dollar3(2));
		moneyList.add(new Dollar3(3));
		moneyList.add(new Euro(1));
		moneyList.add(new Euro(2));
		moneyList.add(new Euro(3));
		System.out.println(moneyList);
		Dollar3.setYenRate(80);
		Euro.setYenRate(115);
		Collections.sort(moneyList);
		System.out.println(moneyList);
		List<Money3> moneyList2 = new ArrayList<Money3>();
		for(Money3 m : moneyList){
			moneyList2.add(m.getYen());
		}
		System.out.println(moneyList2);
	}
}
