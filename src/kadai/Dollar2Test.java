package kadai;

import static org.junit.Assert.*;

import org.junit.Test;

public class Dollar2Test {

	@Test
	public void testSetYenRate() throws ClassNotFoundException {
		Money2 m = new Dollar2(3);
		assertEquals("$3.0", m.toString());
		Dollar2.setYenRate(80.0);
		Money2 m2 = m.getYen();
		assertEquals(Class.forName("kadai.Yen2"),m2.getClass());
		assertEquals("240.0円",m2.toString());
		Dollar2.setYenRate(100.0);
		Money2 m3 = m.getYen();
		assertEquals("300.0円",m3.toString());
	}

	@Test
	public void testToString() {
		Money2 m = new Dollar2(3);
		assertEquals("$3.0", m.toString());
	}

}
